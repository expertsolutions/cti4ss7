﻿namespace CTIExample
{
	partial class TestForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.phoneTextBox = new System.Windows.Forms.MaskedTextBox();
			this.loginButton = new System.Windows.Forms.Button();
			this.acceptButton = new System.Windows.Forms.Button();
			this.rejectButton = new System.Windows.Forms.Button();
			this.phoneLabel = new System.Windows.Forms.Label();
			this.releaseButton = new System.Windows.Forms.Button();
			this.transferButton = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.transferTextBox = new System.Windows.Forms.MaskedTextBox();
			this.scriptLabel = new System.Windows.Forms.Label();
			this.operatorStateLabel = new System.Windows.Forms.Label();
			this.pinTextBox = new System.Windows.Forms.MaskedTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.autoAnswer = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(115, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Мобильный телефон:";
			// 
			// phoneTextBox
			// 
			this.phoneTextBox.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
			this.phoneTextBox.Location = new System.Drawing.Point(133, 5);
			this.phoneTextBox.Mask = "(000) 000-0000";
			this.phoneTextBox.Name = "phoneTextBox";
			this.phoneTextBox.Size = new System.Drawing.Size(101, 20);
			this.phoneTextBox.TabIndex = 1;
			this.phoneTextBox.Text = "9037136054";
			this.phoneTextBox.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
			// 
			// loginButton
			// 
			this.loginButton.Location = new System.Drawing.Point(249, 4);
			this.loginButton.Name = "loginButton";
			this.loginButton.Size = new System.Drawing.Size(75, 23);
			this.loginButton.TabIndex = 2;
			this.loginButton.Text = "Login";
			this.loginButton.UseVisualStyleBackColor = true;
			this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
			// 
			// acceptButton
			// 
			this.acceptButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.acceptButton.Enabled = false;
			this.acceptButton.Location = new System.Drawing.Point(4, 162);
			this.acceptButton.Name = "acceptButton";
			this.acceptButton.Size = new System.Drawing.Size(156, 23);
			this.acceptButton.TabIndex = 3;
			this.acceptButton.Text = "Принять вызов";
			this.acceptButton.UseVisualStyleBackColor = true;
			this.acceptButton.Click += new System.EventHandler(this.acceptButton_Click);
			// 
			// rejectButton
			// 
			this.rejectButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.rejectButton.Enabled = false;
			this.rejectButton.Location = new System.Drawing.Point(166, 162);
			this.rejectButton.Name = "rejectButton";
			this.rejectButton.Size = new System.Drawing.Size(156, 23);
			this.rejectButton.TabIndex = 4;
			this.rejectButton.Text = "Отклонить вызов";
			this.rejectButton.UseVisualStyleBackColor = true;
			this.rejectButton.Click += new System.EventHandler(this.rejectButton_Click);
			// 
			// phoneLabel
			// 
			this.phoneLabel.AutoSize = true;
			this.phoneLabel.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.phoneLabel.Location = new System.Drawing.Point(11, 54);
			this.phoneLabel.Name = "phoneLabel";
			this.phoneLabel.Size = new System.Drawing.Size(188, 19);
			this.phoneLabel.TabIndex = 5;
			this.phoneLabel.Text = "Нет входящего вызова";
			// 
			// releaseButton
			// 
			this.releaseButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.releaseButton.Enabled = false;
			this.releaseButton.Location = new System.Drawing.Point(166, 222);
			this.releaseButton.Name = "releaseButton";
			this.releaseButton.Size = new System.Drawing.Size(156, 23);
			this.releaseButton.TabIndex = 7;
			this.releaseButton.Text = "Завершить вызов";
			this.releaseButton.UseVisualStyleBackColor = true;
			this.releaseButton.Click += new System.EventHandler(this.releaseButton_Click);
			// 
			// transferButton
			// 
			this.transferButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.transferButton.Enabled = false;
			this.transferButton.Location = new System.Drawing.Point(4, 222);
			this.transferButton.Name = "transferButton";
			this.transferButton.Size = new System.Drawing.Size(156, 23);
			this.transferButton.TabIndex = 6;
			this.transferButton.Text = "Перевести вызов";
			this.transferButton.UseVisualStyleBackColor = true;
			this.transferButton.Click += new System.EventHandler(this.transferButton_Click);
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(7, 198);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(123, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Номер для трансфера:";
			// 
			// transferTextBox
			// 
			this.transferTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.transferTextBox.Enabled = false;
			this.transferTextBox.Location = new System.Drawing.Point(133, 195);
			this.transferTextBox.Mask = "0000000";
			this.transferTextBox.Name = "transferTextBox";
			this.transferTextBox.Size = new System.Drawing.Size(100, 20);
			this.transferTextBox.TabIndex = 9;
			// 
			// scriptLabel
			// 
			this.scriptLabel.AutoSize = true;
			this.scriptLabel.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.scriptLabel.Location = new System.Drawing.Point(12, 82);
			this.scriptLabel.Name = "scriptLabel";
			this.scriptLabel.Size = new System.Drawing.Size(195, 19);
			this.scriptLabel.TabIndex = 10;
			this.scriptLabel.Text = "ScriptID: не установлен";
			// 
			// operatorStateLabel
			// 
			this.operatorStateLabel.AutoSize = true;
			this.operatorStateLabel.Font = new System.Drawing.Font("Arial Black", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.operatorStateLabel.Location = new System.Drawing.Point(12, 112);
			this.operatorStateLabel.Name = "operatorStateLabel";
			this.operatorStateLabel.Size = new System.Drawing.Size(233, 19);
			this.operatorStateLabel.TabIndex = 11;
			this.operatorStateLabel.Text = "SS7 соединение отсутствует";
			// 
			// pinTextBox
			// 
			this.pinTextBox.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
			this.pinTextBox.Location = new System.Drawing.Point(133, 31);
			this.pinTextBox.Mask = "9999";
			this.pinTextBox.Name = "pinTextBox";
			this.pinTextBox.Size = new System.Drawing.Size(101, 20);
			this.pinTextBox.TabIndex = 13;
			this.pinTextBox.Text = "1234";
			this.pinTextBox.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 35);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(51, 13);
			this.label2.TabIndex = 12;
			this.label2.Text = "Пин-код:";
			// 
			// autoAnswer
			// 
			this.autoAnswer.AutoSize = true;
			this.autoAnswer.Checked = true;
			this.autoAnswer.CheckState = System.Windows.Forms.CheckState.Checked;
			this.autoAnswer.Location = new System.Drawing.Point(16, 139);
			this.autoAnswer.Name = "autoAnswer";
			this.autoAnswer.Size = new System.Drawing.Size(123, 17);
			this.autoAnswer.TabIndex = 14;
			this.autoAnswer.Text = "Автоприем вызова";
			this.autoAnswer.UseVisualStyleBackColor = true;
			// 
			// TestForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(334, 261);
			this.Controls.Add(this.autoAnswer);
			this.Controls.Add(this.pinTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.operatorStateLabel);
			this.Controls.Add(this.scriptLabel);
			this.Controls.Add(this.transferTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.releaseButton);
			this.Controls.Add(this.transferButton);
			this.Controls.Add(this.phoneLabel);
			this.Controls.Add(this.rejectButton);
			this.Controls.Add(this.acceptButton);
			this.Controls.Add(this.loginButton);
			this.Controls.Add(this.phoneTextBox);
			this.Controls.Add(this.label1);
			this.Name = "TestForm";
			this.Text = "Прототип";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TestForm_FormClosed);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.MaskedTextBox phoneTextBox;
		private System.Windows.Forms.Button loginButton;
		private System.Windows.Forms.Button acceptButton;
		private System.Windows.Forms.Button rejectButton;
		private System.Windows.Forms.Label phoneLabel;
		private System.Windows.Forms.Button releaseButton;
		private System.Windows.Forms.Button transferButton;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.MaskedTextBox transferTextBox;
		private System.Windows.Forms.Label scriptLabel;
		private System.Windows.Forms.Label operatorStateLabel;
		private System.Windows.Forms.MaskedTextBox pinTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox autoAnswer;
	}
}

