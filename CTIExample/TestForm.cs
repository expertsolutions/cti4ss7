﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;

namespace CTIExample
{
	public partial class TestForm : Form
	{
		private Socket client				= null; 
		private ManualResetEvent sendDone	= new ManualResetEvent(false);
		private AutoResetEvent receiveDone	= new AutoResetEvent(false);
		private ManualResetEvent programExit = new ManualResetEvent(false);
		private string response				= String.Empty;
		private string scriptAddress		= String.Empty;
		private Thread receiveThread		= null;
		Properties.Settings settings		= Properties.Settings.Default;

		private delegate void IncomingDataDelegate(string text);

		public TestForm()
		{
			InitializeComponent();
			phoneTextBox.Text = settings.SS7Phone;
			restartClientSocket();
		}

		private void loginButton_Click(object sender, EventArgs e)
		{
			if(phoneTextBox.Text.Length != 10)
			{
				MessageBox.Show("Введите номер телефона", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}	

			client.Connect(settings.CTIHostServer, settings.CTIHostPort);

			receiveDone.Set();
			receiveThread = new Thread(this.ReceiveThread);
			receiveThread.Start();

			Send(@"{""source"":""CTI"",""requestId"":""53480824333d4bd485a2e5da7eb071c5"", ""jsondata"":{""user"":""" + settings.Operator + @""", ""pinCode"":""" + pinTextBox.Text + @""", ""comp"":""test"", ""phone"":"""
				+ settings.SIPPhone +
				@""", ""ss7Phone"":""" 
				+ phoneTextBox.Text +
				@""", ""registrarPassword"":""" 
				+ settings.RegistrarPassword + 
				@"""},""methodName"":""init"",""destination"":""CTIHost""}");
			sendDone.WaitOne();
			
			//Receive();
		}

		private void ReceiveThread(object data)
		{
			while (WaitHandle.WaitAny(new WaitHandle[] { receiveDone, programExit}) != 1)
			{
				Log("Receive started");
				Receive();
			}
		}

		private void Log(string entry)
		{
			//System.Diagnostics.EventLog.WriteEntry("CTIExample", entry, System.Diagnostics.EventLogEntryType.Information);
		}

		private void Receive()
		{
			try
			{
				receiveDone.Reset();
				// Create the state object.
				StateObject state = new StateObject();
				state.workSocket = client;
				state.form = this;

				// Begin receiving the data from the remote device.
				client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
					new AsyncCallback(ReceiveCallback), state);
			}
			catch (Exception e)
			{
				Log(e.ToString());
			}
		}

		private void ReceiveCallback(IAsyncResult ar)
		{
			try
			{
				// Retrieve the state object and the client socket 
				// from the asynchronous state object.
				StateObject state = (StateObject)ar.AsyncState;
				Socket client = state.workSocket;

				// Read data from the remote device.
				int bytesRead = client.EndReceive(ar);

				if (bytesRead > 0)
				{
					// There might be more data, so store the data received so far.
					state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, bytesRead));

					response = state.sb.ToString();
					while(response.Contains("\0\0"))
					{
						var index = response.IndexOf("\0\0");
						string value = response.Substring(0, index);
						response = response.Substring(index + 2);
						try
						{
							state.form.Invoke(new IncomingDataDelegate(ProcessIncomingData), new object[] { value });
						}
						catch (Exception e)
						{
							Log(e.ToString());
						}
					}
				}
				receiveDone.Set();
			}
			catch (Exception e)
			{
				Log(e.ToString());
			}
		}

		private void swtichCallButtonsState(bool enabled)
		{
			rejectButton.Enabled = enabled;
			acceptButton.Enabled = enabled;
		}

		private void restartClientSocket()
		{
			if(this.client != null)
				this.client.Close();

			this.client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); 
		}

		private void switchLoginControlsState(bool enabled)
		{
			loginButton.Enabled = enabled;
			phoneTextBox.Enabled = enabled;
			pinTextBox.Enabled = enabled;
		}

		private void switchInCallControlsState(bool enabled)
		{
			transferButton.Enabled = enabled;
			transferTextBox.Enabled = enabled;
			releaseButton.Enabled = enabled;
		}

		private void ProcessIncomingData(string data)
		{
			Log("Incoming data: " + data); //, System.Diagnostics.EventLogEntryType.Information);

			try
			{
				Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(data);// (Newtonsoft.Json.Linq.JObject)JsonConvert.DeserializeObject(data);
				var methodName = json["methodName"].ToString();

				switch (methodName)
				{
					case "initAck":
						switchLoginControlsState(false);
						break;
					case "scriptConnected":
						var scriptID = Convert.ToUInt64(json["OperatorScriptID"].ToString(), 16);
						scriptAddress = json["OperatorScriptID"].ToString();
						scriptLabel.Text = string.Format("ScriptID: {0:X}-{1:X}", scriptID >> 32, scriptID & 0xFFFFFFFF);
						break;
					case "scriptDisconnected":
						scriptLabel.Text = "ScriptID: не установлен";

						switchLoginControlsState(true);
						swtichCallButtonsState(false);
						switchInCallControlsState(false);

						restartClientSocket();
						break;
					case "incomingCall":
						try
						{
							phoneLabel.Text = "Входящий вызов: " + json["userPhone"].ToString();
						}
						catch
						{
						}

						if (autoAnswer.Checked)
							acceptButton_Click(this, new EventArgs());
						else
							swtichCallButtonsState(true);
						break;
					case "userConnected":
						swtichCallButtonsState(false);
						switchInCallControlsState(true);
						break;
					case "userDisconnected":
						switchInCallControlsState(false);
						break;
					case "ss7Connected":
						operatorStateLabel.Text = "Соединение по SS7 установлено";
						break;
					case "ss7Disconnected":
						operatorStateLabel.Text = "SS7 соединение отсутствует";
						break;
				}
			}
			catch
			{

			}
		}

		private void Send(String data)
		{
			Log(data);
			// Convert the string data to byte data using ASCII encoding.
			byte[] byteData = Encoding.UTF8.GetBytes(data + "\0\0");

			// Begin sending the data to the remote device.
			client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), client);
		}

		private void SendCallback(IAsyncResult ar)
		{
			try
			{
				// Retrieve the socket from the state object.
				Socket client = (Socket)ar.AsyncState;

				// Complete sending the data to the remote device.
				int bytesSent = client.EndSend(ar);
				Console.WriteLine("Sent {0} bytes to server.", bytesSent);

				// Signal that all bytes have been sent.
				sendDone.Set();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
			}
		}

		private void TestForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			programExit.Set();
		}

		private void acceptButton_Click(object sender, EventArgs e)
		{
			swtichCallButtonsState(false);
			switchInCallControlsState(true);
			sendCommandToScript("acceptCall");
		}

		private void rejectButton_Click(object sender, EventArgs e)
		{
			processCallDisconnect();
			sendCommandToScript("rejectCall");
		}

		private void sendCommandToScript(string command, string parameters = "")
		{
			StringBuilder cmd = new StringBuilder();
			cmd.Append(@"{""methodName"":""").Append(command).Append(@""", ""source"":""CTI"", ""destination"":""Script"", ""destinationAddress"": """).Append(scriptAddress).Append(@"""");
			if (!string.IsNullOrEmpty(parameters))
				cmd.Append(",").Append(parameters);
			cmd.Append("}");
			var sendValue = cmd.ToString();
			Send(sendValue);
		}

		private void transferButton_Click(object sender, EventArgs e)
		{
			processCallDisconnect();
			sendCommandToScript("transferCall", string.Format(@"""transferNumber"": ""{0}""", transferTextBox.Text));
		}

		private void releaseButton_Click(object sender, EventArgs e)
		{
			processCallDisconnect();
			sendCommandToScript("endCall");
		}

		private void processCallDisconnect()
		{
			phoneLabel.Text = "Нет входящего вызова";
			swtichCallButtonsState(false);
			switchInCallControlsState(false);
		}
	}
}
